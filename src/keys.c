/*
 * Copyright © 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Tumult.
 *
 * Tumult is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Tumult is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE // for strcasestr

#include <gconf/gconf-client.h>
#include <gdk/gdkx.h>
#include <gtk/gtk.h>
#include "keys.h"
#include "player.h"
#include <stdbool.h>
#include <string.h>
#include <strings.h>

#define KEYS_ENTRY "/apps/tumult/keys"

#define KEY_COUNT 4

static int keycodes[KEY_COUNT];
static unsigned int modmasks[KEY_COUNT];
static char *keykeys[KEY_COUNT] = {
	"status",
	"playpause",
	"next",
	"prev"
};
static void (*callbacks[KEY_COUNT])() = {
	show_status,
	play_pause,
	next,
	prev
};

static bool
parse_keybind(const gchar *str, int *key, unsigned int *mod_mask)
{
	const gchar *keystr = strrchr(str, '>');
	keystr = keystr ? keystr + 1 : str;

	KeySym sym = XStringToKeysym(keystr);
	if (sym == NoSymbol)
		return FALSE;

	*key = XKeysymToKeycode(GDK_DISPLAY(), sym);

	*mod_mask = 0;

	if (strcasestr(str, "<alt>"))
		*mod_mask |= Mod1Mask;
	if (strcasestr(str, "<shift>"))
		*mod_mask |= ShiftMask;
	if (strcasestr(str, "<ctrl>") || strcasestr(str, "<control>"))
		*mod_mask |= ControlMask;
	if (strcasestr(str, "<win>") || strcasestr(str, "<super>"))
		*mod_mask |= Mod5Mask;
	if (strcasestr(str, "<any>"))
		*mod_mask = AnyModifier;

	return TRUE;
}

static void
grab(Display *dpy, int key, unsigned int mods, Window root)
{
	XGrabKey(dpy, key, mods, root,
	         True, GrabModeAsync, GrabModeAsync);
}

static void
ungrab(Display *dpy, int key, unsigned int mods, Window root)
{
	XUngrabKey(dpy, key, mods, root);
}

static void
grab_key(int i, const gchar *str)
{
	gdk_error_trap_push();

	GdkDisplay *display = gdk_display_get_default();
	Display *dpy = GDK_DISPLAY_XDISPLAY(display);
	GdkScreen *screen;
	Window root;
	int s = gdk_display_get_n_screens(display);
	void (*do_grab)(Display *dpy, int key, unsigned int mods, Window root) = str ? grab : ungrab;

	while (s--)
		if ((screen = gdk_display_get_screen(display, s))) {
			root = GDK_WINDOW_XID(gdk_screen_get_root_window(screen));
			if (modmasks[i] == AnyModifier)
				do_grab(dpy, keycodes[i], modmasks[i], root);
			else {
				do_grab(dpy, keycodes[i], modmasks[i], root);
				do_grab(dpy, keycodes[i], modmasks[i] | Mod2Mask, root);
				do_grab(dpy, keycodes[i], modmasks[i] | Mod5Mask, root);
				do_grab(dpy, keycodes[i], modmasks[i] | LockMask, root);
				do_grab(dpy, keycodes[i], modmasks[i] | Mod2Mask | Mod5Mask, root);
				do_grab(dpy, keycodes[i], modmasks[i] | Mod2Mask | LockMask, root);
				do_grab(dpy, keycodes[i], modmasks[i] | Mod5Mask | LockMask, root);
				do_grab(dpy, keycodes[i], modmasks[i] | Mod2Mask | Mod5Mask | LockMask, root);
			}
		}

	gdk_flush();
	if (gdk_error_trap_pop() && str)
		fprintf(stderr, "Error grabbing key '%s'\n", str);
}

static int
key_to_index(const gchar *key)
{
	int i;
	for (i = 0; i != KEY_COUNT; ++i)
		if (!strcmp(key, keykeys[i]))
			return i;
	return -1;
}

static void
key_changed_callback(GConfClient *client, guint cnxn_id, GConfEntry *entry, gpointer data)
{
	int i = key_to_index(strrchr(entry->key, '/') + 1);
	if (i == -1)
		return;

	grab_key(i, NULL);

	if (!entry->value)
		goto empty;

	const gchar *v = gconf_value_get_string(entry->value);
	if (*v == '\0')
		goto empty;

	if (parse_keybind(v, keycodes + i, modmasks + i)) {
		grab_key(i, v);
		return;
	} else
		fprintf(stderr, "Could not parse string '%s' into keysym\n", v);

empty:
	keycodes[i] = modmasks[i] = 0;
}

static GdkFilterReturn
key_filter(GdkXEvent *xevent, GdkEvent *event, gpointer data)
{
	if (((XEvent*)xevent)->type == KeyPress) {
		XKeyEvent *key = (XKeyEvent*)xevent;

		int i;
		int state = key->state & ~(Mod2Mask | Mod5Mask | LockMask);
		for (i = 0; i != KEY_COUNT; ++i)
			if (key->keycode == keycodes[i] && (modmasks[i] == AnyModifier || state == modmasks[i])) {
				(callbacks[i])();
				return GDK_FILTER_REMOVE;
			}
	}

	return GDK_FILTER_CONTINUE;
}

void
keys_init()
{
	GConfClient* client = gconf_client_get_default();

	gconf_client_add_dir(client, KEYS_ENTRY, GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);
	gconf_client_notify_add(client, KEYS_ENTRY, key_changed_callback, NULL, NULL, NULL);

	int i;
	gchar *key;
	gchar *str;

	for (i = 0; i != KEY_COUNT; ++i) {
		key = g_strconcat(KEYS_ENTRY, "/", keykeys[i], NULL);
		str = gconf_client_get_string(client, key, NULL);
		if (str && parse_keybind(str, keycodes + i, modmasks + i))
			grab_key(i, str);
		else
			gconf_client_unset(client, key, NULL);
		g_free(key);
		g_free(str);
	}

	GdkDisplay *display = gdk_display_get_default();
	GdkScreen *screen;
	i = gdk_display_get_n_screens(display);

	while (i--)
		if ((screen = gdk_display_get_screen(display, i)))
			gdk_window_add_filter(gdk_screen_get_root_window(screen), key_filter, NULL);

	g_object_unref(client);
}
