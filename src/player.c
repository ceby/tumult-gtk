/*
 * Copyright © 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Tumult.
 *
 * Tumult is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Tumult is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <gst/gst.h>
#include "message.h"
#include "player.h"
#include <stdbool.h>
#include <string.h>

static bool playing;
static gchar *current_title = NULL;
static gchar *current_artist = NULL;
static const gchar *current_stream_name;

static char **streams;
static int streamc;
static int *urics;

static int current_stream_i;
static int current_uri_i;
static char *current_uri;

static guint retry_timeout = 0;
static unsigned char retry_attempts = 0;

static GstElement *pipeline;

static void
set_status(gchar *title, gchar *artist)
{
	g_free(current_title);
	g_free(current_artist);
	current_title = title;
	current_artist = artist;
}

static void
stop()
{
	gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_READY);
	set_status(NULL, NULL);
	playing = FALSE;
}

static void
play()
{
	gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_PLAYING);
	playing = TRUE;
}

static void
next_uri()
{
	if (++current_uri_i == urics[current_stream_i])
		current_uri_i = 0;

	if (current_uri_i == 0)
		current_uri = streams[current_stream_i];

	current_uri = strchr(current_uri, '\0') + 1;

	if (playing) {
		stop();
		g_object_set(G_OBJECT(pipeline), "uri", current_uri, NULL);
		play();
	} else
		g_object_set(G_OBJECT(pipeline), "uri", current_uri, NULL);
}

static void
seek(int i, bool init)
{
	if (playing || init) {
		current_stream_i = i;
		current_stream_name = streams[i];

		current_uri_i = -1;
		next_uri();
	} else
		play();

	if (playing)
		show_status();
}

static gboolean
retry_cb(gpointer data)
{
	retry_timeout = 0;
	next_uri();
	return FALSE;
}

static void
retry(const gchar *msg)
{
	if (retry_timeout)
		return;

	gchar *text;
	if (++retry_attempts == 3) {
		text = g_markup_printf_escaped("%s. Retried 3 times. Giving up.", msg);
		retry_attempts = 0;
		stop();
	} else {
		text = g_markup_printf_escaped("%s. Retrying in 5 seconds...", msg);
		retry_timeout = g_timeout_add_seconds(5, retry_cb, NULL);
	}

	show_message(text);
	g_free(text);
}

static gboolean
bus_call(GstBus *bus, GstMessage *msg, gpointer user_data)
{
	switch (GST_MESSAGE_TYPE(msg)) {
	case GST_MESSAGE_EOS:
		next_uri();
		break;
	case GST_MESSAGE_ERROR: {
		GError *err;

		gst_message_parse_error(msg, &err, NULL);

		if (err->domain == GST_RESOURCE_ERROR)
			switch (err->code) {
			case GST_RESOURCE_ERROR_NOT_FOUND:
			case GST_RESOURCE_ERROR_READ:
			case GST_RESOURCE_ERROR_OPEN_READ:
				retry(err->message);
				goto freeness;
			}

		g_error("%s", err->message);

	freeness:
		g_error_free(err);
		break;
	}
	case GST_MESSAGE_TAG: {
		GstTagList *tags;
		gchar *artist;
		gchar *title;
		gchar *tag_title;
		gchar **info = NULL;

		gst_message_parse_tag(msg, &tags);
		if (gst_tag_list_get_string(tags, GST_TAG_TITLE, &tag_title)) {
			if (gst_tag_list_get_string(tags, GST_TAG_ARTIST, &artist))
				title = tag_title;
			else {
				info = g_strsplit(tag_title, " - ", 0);
				artist = info[0];
				title = info[1];
			}

			if (!current_title || strcmp(title, current_title))
				set_status(g_strdup(title), g_strdup(artist));

			if (info)
				g_strfreev(info);
			else
				g_free(artist);
			g_free(tag_title);
		}
		gst_tag_list_free(tags);
		break;
	}
	default:
		break;
	}

	return TRUE;
}

void
show_status()
{
	if (playing) {
		if (current_title) {
			gchar *text;
			text = g_markup_printf_escaped("<b>%s</b>\n%s\n%s", current_title, current_artist, current_stream_name);
			show_message(text);
			g_free(text);
		} else
			show_message(current_stream_name);
	} else
		show_message("Playback Stopped");
}

void
next()
{
	seek(current_stream_i == streamc - 1 ? 0 : current_stream_i + 1, FALSE);
}

void
prev()
{
	seek((current_stream_i == 0 ? streamc : current_stream_i) - 1, FALSE);
}

void
play_pause()
{
	if (playing)
		stop();
	else
		play();

	show_status();
}

void
player_init(int argc, char **argv)
{
	streams = argv + 1;
	streamc = -1;
	playing = TRUE;

	int i;
	char *end;
	int *cs;
	for (i = 1; i != argc; ++i)
		if (argv[i][0] == '-') {
			if (streamc > 0) {
				fprintf(stderr, "Options must preceed streams in arguments.\n");
				exit(EXIT_FAILURE);
			}
			--streamc;
			++streams;
			if (argv[i][1] == 'q')
				playing = FALSE;
		} else {
			if (streamc < 0) {
				streamc += argc;
				cs = urics = malloc(sizeof(int) * streamc);
			}

			end = argv[i] - 1;
			*cs = 0;
			while ((end = strchr(end + 1, '!'))) {
				*end = '\0';
				*cs += 1;
			}

			if (*cs == 0) {
				fprintf(stderr, "No URI(s) specified for stream '%s'.\n", argv[i]);
				exit(EXIT_FAILURE);
			}

			++cs;
		}

	pipeline = gst_element_factory_make("playbin2", "player");

	GstBus *bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
	gst_bus_add_watch(bus, bus_call, NULL);
	gst_object_unref(bus);

	seek(0, TRUE);
}
