/*
 * Copyright © 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Tumult.
 *
 * Tumult is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Tumult is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <gconf/gconf.h>
#include <gst/gst.h>
#include <gtk/gtk.h>
#include "keys.h"
#include "player.h"

int
main(int argc, char **argv)
{
	gtk_init(&argc, &argv);
	gconf_init(argc, argv, NULL);
	gst_init(&argc, &argv);

	if (argc == 1) {
		fprintf(stderr, "Usage: %s 'Stream Name!uri1!uri2' 'Stream Name!uri' ...\n", argv[0]);
		return 0;
	}

	keys_init();
	player_init(argc, argv);

	gtk_main();

	return 0;
}
