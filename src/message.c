/*
 * Copyright © 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Tumult.
 *
 * Tumult is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Tumult is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h>
#include "message.h"

static GtkWidget *window = NULL;
static GtkWidget *label;
static guint timeout_id;

static gboolean
status_close(gpointer data)
{
	if (window) {
		gtk_widget_destroy(window);
		window = NULL;
	}
	return FALSE;
}

void
show_message(const gchar *text)
{
	if (window)
		g_source_remove(timeout_id);

	GtkWidget *win = gtk_window_new(GTK_WINDOW_POPUP);
	gtk_window_set_gravity(GTK_WINDOW(win), GDK_GRAVITY_NORTH_EAST);
	gtk_container_set_border_width(GTK_CONTAINER(win), 5);

	label = gtk_label_new(NULL);
	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_CENTER);

	GdkColor white = { 0, 65535, 65535, 65535 };
	GdkColor black = { 0, 0, 0, 0 };
	gtk_widget_modify_fg(label, GTK_STATE_NORMAL, &white);
	gtk_widget_modify_bg(win, GTK_STATE_NORMAL, &black);

	gtk_container_add(GTK_CONTAINER(win), label);

	gtk_label_set_markup(GTK_LABEL(label), text);

	PangoRectangle rect;
	pango_layout_get_extents(gtk_label_get_layout(GTK_LABEL(label)), NULL, &rect);
	pango_extents_to_pixels(&rect, NULL);
	gtk_window_move(GTK_WINDOW(win), gdk_screen_width() - rect.width - 15, 5);

	gtk_widget_show_all(win);

	if (window)
		gtk_widget_destroy(window);

	window = win;
	timeout_id = g_timeout_add_seconds(3, status_close, NULL);
}
