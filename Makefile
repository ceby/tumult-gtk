# because autotools is scary...

SUBDIRS = src
DEST = /usr/local/bin

.PHONY: clean subdirs clean strip install $(SUBDIRS)

subdirs: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@

clean:
	$(foreach sub, $(SUBDIRS), make -C $(sub) clean)

install: subdirs
	install -sm 755 tumult $(DEST)
